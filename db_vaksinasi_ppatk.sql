-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3309
-- Generation Time: Jun 16, 2021 at 11:34 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_vaksinasi_ppatk`
--

-- --------------------------------------------------------

--
-- Table structure for table `cc_options`
--

CREATE TABLE `cc_options` (
  `id` int(11) UNSIGNED NOT NULL,
  `option_name` varchar(200) NOT NULL,
  `option_value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_options`
--

INSERT INTO `cc_options` (`id`, `option_name`, `option_value`) VALUES
(1, 'active_theme', 'cicool'),
(2, 'favicon', 'default.png'),
(3, 'site_name', 'PPATK'),
(4, 'enable_disqus', NULL),
(5, 'disqus_id', ''),
(6, 'email', 'nurmansetyawan@gmail.com'),
(7, 'author', ''),
(8, 'site_description', ''),
(9, 'keywords', ''),
(10, 'landing_page_id', 'default'),
(11, 'timezone', 'Asia/Jakarta'),
(12, 'google_id', ''),
(13, 'google_secret', '');

-- --------------------------------------------------------

--
-- Table structure for table `m_batch`
--

CREATE TABLE `m_batch` (
  `id_batch` tinyint(4) NOT NULL,
  `desc_batch` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_batch`
--

INSERT INTO `m_batch` (`id_batch`, `desc_batch`) VALUES
(1, '07.00 - 09.00'),
(2, '09.00 - 10.00'),
(3, ' 10.00 - 11.00'),
(4, ' 11.00 - 12.00'),
(5, '12.30 - 13.00 '),
(6, ' 13.00 - 14.00 '),
(7, ' 14.00 - 15.00'),
(8, ' 15.00 - 16.00');

-- --------------------------------------------------------

--
-- Table structure for table `m_gender`
--

CREATE TABLE `m_gender` (
  `id_gender` tinyint(1) NOT NULL,
  `gender` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_gender`
--

INSERT INTO `m_gender` (`id_gender`, `gender`) VALUES
(1, 'L'),
(2, 'P');

-- --------------------------------------------------------

--
-- Table structure for table `m_venue`
--

CREATE TABLE `m_venue` (
  `id_venue` tinyint(4) NOT NULL,
  `venue` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_venue`
--

INSERT INTO `m_venue` (`id_venue`, `venue`) VALUES
(1, 'VENUE 1'),
(2, 'VENUE 2');

-- --------------------------------------------------------

--
-- Table structure for table `vaksinasi`
--

CREATE TABLE `vaksinasi` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  `no_antrian` int(11) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `venue` varchar(10) NOT NULL,
  `is_registrasi` tinyint(1) NOT NULL DEFAULT '0',
  `registrasi_time` time DEFAULT NULL,
  `is_venue` tinyint(1) NOT NULL DEFAULT '0',
  `venue_time` time DEFAULT NULL,
  `is_lolos` tinyint(4) NOT NULL DEFAULT '0',
  `is_vaksinasi` tinyint(4) NOT NULL DEFAULT '0',
  `vaksinasi_time` time DEFAULT NULL,
  `is_observasi` tinyint(4) NOT NULL DEFAULT '0',
  `observasi_time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vaksinasi`
--

INSERT INTO `vaksinasi` (`id`, `nama`, `batch`, `no_antrian`, `gender`, `venue`, `is_registrasi`, `registrasi_time`, `is_venue`, `venue_time`, `is_lolos`, `is_vaksinasi`, `vaksinasi_time`, `is_observasi`, `observasi_time`) VALUES
(1, 'ARIEF KURNIAWAN ', 8, 0, '1', '1', 0, NULL, 0, NULL, 0, 0, NULL, 0, NULL),
(2, 'FAYOTA PRACHMASETIAWAN ', 1, 0, '1', '1', 0, NULL, 0, NULL, 0, 0, NULL, 0, NULL),
(3, 'NURMAN SETYAWAN ', 8, 0, '1', '1', 0, NULL, 0, NULL, 0, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_belum_registrasi`
-- (See below for the actual view)
--
CREATE TABLE `v_belum_registrasi` (
`nama` varchar(255)
,`batch` int(11)
,`gender` varchar(50)
,`venue` varchar(512)
,`desc_batch` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_lobby`
-- (See below for the actual view)
--
CREATE TABLE `v_lobby` (
`nama` varchar(255)
,`batch` int(11)
,`gender` varchar(50)
,`registrasi_time` time
,`venue` varchar(512)
,`desc_batch` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_observasi`
-- (See below for the actual view)
--
CREATE TABLE `v_observasi` (
`nama` varchar(255)
,`batch` int(11)
,`gender` varchar(50)
,`observasi_time` time
,`venue` varchar(512)
,`desc_batch` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_registrasi`
-- (See below for the actual view)
--
CREATE TABLE `v_registrasi` (
`nama` varchar(255)
,`batch` int(11)
,`gender` varchar(50)
,`registrasi_time` time
,`venue` varchar(512)
,`desc_batch` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_sedang_observasi`
-- (See below for the actual view)
--
CREATE TABLE `v_sedang_observasi` (
`nama` varchar(255)
,`batch` int(11)
,`gender` varchar(50)
,`observasi_time` time
,`venue` varchar(512)
,`desc_batch` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_tunda`
-- (See below for the actual view)
--
CREATE TABLE `v_tunda` (
`nama` varchar(255)
,`batch` int(11)
,`gender` varchar(50)
,`venue` varchar(512)
,`desc_batch` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_vaksinasi`
-- (See below for the actual view)
--
CREATE TABLE `v_vaksinasi` (
`nama` varchar(255)
,`batch` int(11)
,`gender` varchar(50)
,`vaksinasi_time` time
,`venue` varchar(512)
,`desc_batch` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_venue`
-- (See below for the actual view)
--
CREATE TABLE `v_venue` (
`nama` varchar(255)
,`batch` int(11)
,`gender` varchar(50)
,`venue_time` time
,`venue` varchar(512)
,`desc_batch` varchar(50)
);

-- --------------------------------------------------------

--
-- Structure for view `v_belum_registrasi`
--
DROP TABLE IF EXISTS `v_belum_registrasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_belum_registrasi`  AS  select `v`.`nama` AS `nama`,`v`.`batch` AS `batch`,`g`.`gender` AS `gender`,`ve`.`venue` AS `venue`,`b`.`desc_batch` AS `desc_batch` from (((`vaksinasi` `v` join `m_batch` `b` on((`v`.`batch` = `b`.`id_batch`))) join `m_gender` `g` on((`v`.`gender` = `g`.`id_gender`))) join `m_venue` `ve` on((`v`.`venue` = `ve`.`id_venue`))) where ((`v`.`is_registrasi` = 0) and (`v`.`is_lolos` = 0)) order by `v`.`nama` ;

-- --------------------------------------------------------

--
-- Structure for view `v_lobby`
--
DROP TABLE IF EXISTS `v_lobby`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_lobby`  AS  select `v`.`nama` AS `nama`,`v`.`batch` AS `batch`,`g`.`gender` AS `gender`,`v`.`registrasi_time` AS `registrasi_time`,`ve`.`venue` AS `venue`,`b`.`desc_batch` AS `desc_batch` from (((`vaksinasi` `v` join `m_batch` `b` on((`v`.`batch` = `b`.`id_batch`))) join `m_gender` `g` on((`v`.`gender` = `g`.`id_gender`))) join `m_venue` `ve` on((`v`.`venue` = `ve`.`id_venue`))) where ((`v`.`is_registrasi` = 1) and (`v`.`is_venue` = 0) and (`v`.`is_vaksinasi` = 0) and (`v`.`is_observasi` = 0)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_observasi`
--
DROP TABLE IF EXISTS `v_observasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_observasi`  AS  select `v`.`nama` AS `nama`,`v`.`batch` AS `batch`,`g`.`gender` AS `gender`,`v`.`observasi_time` AS `observasi_time`,`ve`.`venue` AS `venue`,`b`.`desc_batch` AS `desc_batch` from (((`vaksinasi` `v` join `m_batch` `b` on((`v`.`batch` = `b`.`id_batch`))) join `m_gender` `g` on((`v`.`gender` = `g`.`id_gender`))) join `m_venue` `ve` on((`v`.`venue` = `ve`.`id_venue`))) where ((`v`.`is_registrasi` = 1) and (`v`.`is_venue` = 1) and (`v`.`is_vaksinasi` = 1) and (`v`.`is_observasi` = 1)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_registrasi`
--
DROP TABLE IF EXISTS `v_registrasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_registrasi`  AS  select `v`.`nama` AS `nama`,`v`.`batch` AS `batch`,`g`.`gender` AS `gender`,`v`.`registrasi_time` AS `registrasi_time`,`ve`.`venue` AS `venue`,`b`.`desc_batch` AS `desc_batch` from (((`vaksinasi` `v` join `m_batch` `b` on((`v`.`batch` = `b`.`id_batch`))) join `m_gender` `g` on((`v`.`gender` = `g`.`id_gender`))) join `m_venue` `ve` on((`v`.`venue` = `ve`.`id_venue`))) where (`v`.`is_registrasi` = 1) ;

-- --------------------------------------------------------

--
-- Structure for view `v_sedang_observasi`
--
DROP TABLE IF EXISTS `v_sedang_observasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sedang_observasi`  AS  select `v`.`nama` AS `nama`,`v`.`batch` AS `batch`,`g`.`gender` AS `gender`,`v`.`observasi_time` AS `observasi_time`,`ve`.`venue` AS `venue`,`b`.`desc_batch` AS `desc_batch` from (((`vaksinasi` `v` join `m_batch` `b` on((`v`.`batch` = `b`.`id_batch`))) join `m_gender` `g` on((`v`.`gender` = `g`.`id_gender`))) join `m_venue` `ve` on((`v`.`venue` = `ve`.`id_venue`))) where ((`v`.`is_registrasi` = 1) and (`v`.`is_venue` = 1) and (`v`.`is_vaksinasi` = 1) and (`v`.`is_observasi` = 0)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_tunda`
--
DROP TABLE IF EXISTS `v_tunda`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_tunda`  AS  select `v`.`nama` AS `nama`,`v`.`batch` AS `batch`,`g`.`gender` AS `gender`,`ve`.`venue` AS `venue`,`b`.`desc_batch` AS `desc_batch` from (((`vaksinasi` `v` join `m_batch` `b` on((`v`.`batch` = `b`.`id_batch`))) join `m_gender` `g` on((`v`.`gender` = `g`.`id_gender`))) join `m_venue` `ve` on((`v`.`venue` = `ve`.`id_venue`))) where (`v`.`is_lolos` = 1) ;

-- --------------------------------------------------------

--
-- Structure for view `v_vaksinasi`
--
DROP TABLE IF EXISTS `v_vaksinasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_vaksinasi`  AS  select `v`.`nama` AS `nama`,`v`.`batch` AS `batch`,`g`.`gender` AS `gender`,`v`.`vaksinasi_time` AS `vaksinasi_time`,`ve`.`venue` AS `venue`,`b`.`desc_batch` AS `desc_batch` from (((`vaksinasi` `v` join `m_batch` `b` on((`v`.`batch` = `b`.`id_batch`))) join `m_gender` `g` on((`v`.`gender` = `g`.`id_gender`))) join `m_venue` `ve` on((`v`.`venue` = `ve`.`id_venue`))) where (`v`.`is_vaksinasi` = 1) ;

-- --------------------------------------------------------

--
-- Structure for view `v_venue`
--
DROP TABLE IF EXISTS `v_venue`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_venue`  AS  select `v`.`nama` AS `nama`,`v`.`batch` AS `batch`,`g`.`gender` AS `gender`,`v`.`venue_time` AS `venue_time`,`ve`.`venue` AS `venue`,`b`.`desc_batch` AS `desc_batch` from (((`vaksinasi` `v` join `m_batch` `b` on((`v`.`batch` = `b`.`id_batch`))) join `m_gender` `g` on((`v`.`gender` = `g`.`id_gender`))) join `m_venue` `ve` on((`v`.`venue` = `ve`.`id_venue`))) where ((`v`.`is_registrasi` = 1) and (`v`.`is_venue` = 1) and (`v`.`is_vaksinasi` = 0) and (`v`.`is_observasi` = 0) and (`v`.`is_lolos` = 0)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cc_options`
--
ALTER TABLE `cc_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_batch`
--
ALTER TABLE `m_batch`
  ADD PRIMARY KEY (`id_batch`);

--
-- Indexes for table `vaksinasi`
--
ALTER TABLE `vaksinasi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cc_options`
--
ALTER TABLE `cc_options`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `vaksinasi`
--
ALTER TABLE `vaksinasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=525;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
