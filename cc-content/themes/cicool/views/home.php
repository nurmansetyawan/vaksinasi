<!DOCTYPE html>
<html lang="en">

<head>
	<title>DASHBOARD VAKSINASI PPATK</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?= BASE_ASSET; ?>/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>/login_v15/vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>/login_v15/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>/login_v15/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>/login_v15/vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>/login_v15/vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>/login_v15/vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>/login_v15/vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>/login_v15/vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>/login_v15/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>/login_v15/css/main.css">
	<!--===============================================================================================-->
</head>

<body>
	<div class="limiter">
		<div class="container-login100">
				<div class="login100-form-title" style="background-image: url(<?= BASE_ASSET; ?>/login_v15/images/bg-03.jpg);">
					<img src="<?= BASE_URL('uploads/depan.png') ?>" height="200px"><br>
					<span class="login100-form-title-1">
						DASHBOARD VAKSINASI PPATK
					</span>
						<a href="<?= site_url('administrator/ppatk_dashboard/'); ?>" type="button" class="btn btn-outline-primary" aria-pressed="true">Masuk Ke Dashboard</a>
				</div>
		</div>
	</div>


	<!--===============================================================================================-->
	<script src="<?= BASE_ASSET; ?>/login_v15/vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="<?= BASE_ASSET; ?>/login_v15/vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="<?= BASE_ASSET; ?>/login_v15/vendor/bootstrap/js/popper.js"></script>
	<script src="<?= BASE_ASSET; ?>/login_v15/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="<?= BASE_ASSET; ?>/login_v15/vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="<?= BASE_ASSET; ?>/login_v15/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?= BASE_ASSET; ?>/login_v15/vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="<?= BASE_ASSET; ?>/login_v15/vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="<?= BASE_ASSET; ?>/login_v15/js/main.js"></script>

</body>

</html>
