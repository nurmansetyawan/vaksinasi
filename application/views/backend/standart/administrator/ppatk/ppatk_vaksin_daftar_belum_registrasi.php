<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>

<style>
p {
  color: white !important;
}

h3 {
  color: white !important;
}
</style>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-2 col-6">
      <!-- small box -->
      <div class="small-box bg-info">
        <div class="inner">
          <?php
            $total_sudah_registrasi = $this->db->select('*')->from('v_registrasi')->count_all_results();
            $total_belum_registrasi = $this->db->select('*')->from('v_belum_registrasi')->count_all_results();
          ?>
          <h3><?php echo $total_sudah_registrasi . ' / ' . $total_belum_registrasi ?></h3>

          <p><b>TELAH HADIR / BELUM HADIR</b></p>
        </div>
        <div class="icon">
          <i class="ion ion-ios-people"></i>
        </div>
        <a href="<?= BASE_URL('administrator/ppatk_vaksin_daftar_registrasi') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-2 col-6">
      <!-- small box -->
      <div class="small-box bg-secondary">
        <div class="inner">
          <h3><?php echo $total_registrasi = $this->db->select('*')->from('v_lobby')->count_all_results(); ?></h3>

          <p><b>ANTRIAN LOBBY</b></p>
        </div>
        <div class="icon">
          <i class="ion ion-ios-timer"></i>
        </div>
        <a href="<?= BASE_URL('administrator/ppatk_vaksin_daftar_lobby') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->


    <div class="col-lg-2 col-6">
      <!-- small box -->
      <div class="small-box bg-primary">
        <div class="inner">
          <h3><?php echo $total_venue = $this->db->select('*')->from('v_venue')->count_all_results(); ?></h3>

          <p><b>DI VENUE</b></p>
        </div>
        <div class="icon">
          <i class="ion ion-thermometer"></i>
        </div>
          <a href="<?= BASE_URL('administrator/ppatk_vaksin_daftar_venue') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->

    <!-- ./col -->
    <div class="col-lg-2 col-6">
      <!-- small box -->
      <div class="small-box bg-warning">
        <div class="inner">
          <h3><?php echo $total_vaksinasi = $this->db->select('*')->from('v_vaksinasi')->count_all_results(); ?></h3>

          <p><b>TELAH VAKSINASI</b></p>
        </div>
        <div class="icon">
          <i class="ion ion-erlenmeyer-flask"></i>
        </div>
          <a href="<?= BASE_URL('administrator/ppatk_vaksin_daftar_vaksinasi') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <!-- ./col -->
    <div class="col-lg-2 col-6">
      <!-- small box -->
      <div class="small-box bg-danger">
        <div class="inner">
          <h3><?php echo $total_tunda = $this->db->select('*')->from('v_tunda')->count_all_results(); ?></h3>

          <p><b>DITUNDA</b></p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
          <a href="<?= BASE_URL('administrator/ppatk_vaksin_daftar_tunda') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-2 col-6">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3><?php echo $total_observasi= $this->db->select('*')->from('v_observasi')->count_all_results(); ?></h3>

          <p><b>SELESAI OBSERVASI</b></p>
        </div>
        <div class="icon">
          <i class="ion ion-ios-people"></i>
        </div>
          <a href="<?= BASE_URL('administrator/ppatk_vaksin_daftar_observasi') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>

   <div class="row">
     <div class="col-md-2">
       <a href="<?= BASE_URL('administrator/ppatk_vaksin_daftar_registrasi') ?>" class="small-box-footer">
         <div class="info-box mb-3">
            <p class="info-box-icon bg-secondary elevation-1"><?php echo $total_sudah_registrasi; ?></p>
           <div class="info-box-content">
             <span class="info-box-text"><b>SUDAH REGISTRASI</b></span>
           </div>
           <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
       </a>
     </div>
      <div class="col-md-8">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                        <img src="<?= BASE_URL('uploads/depan.png') ?>">
                     </div>
                     <!-- /.widget-user-image -->
                     <h1 class="widget-user-username">DASHBOARD VAKSINASI PPATK - DAFTAR BELUM REGISTRASI/HADIR</h1>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>

                  <input type="hidden" name="bulk" id="bulk" value="delete">
                  <div class="box-body">
                    <table id="example" class="table table-bordered table-striped table-hover">
                      <thead>
                      <tr>
                        <th width="5%">Nomor</th>
                        <th>Nama Pegawai</th>
                        <th>Batch</th>
                        <th>Waktu Batch</th>
                        <th>Jenis Kelamin</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php
                      $no = 0;
                      $registrasis = $this->db->select('*')->from('v_belum_registrasi')->order_by('nama', 'asc')->get()->result();
                      foreach ($registrasis as $registrasi):
                        ++$no;
                      ?>
                      <tr>
                        <td><?=$no;?></td>
                        <td><?=$registrasi->nama;?></td>
                        <td><?=$registrasi->batch;?></td>
                        <td><?=$registrasi->desc_batch;?></td>
                        <td><?=$registrasi->gender;?></td>
                      </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
      <div class="col-md-2">
        <a href="<?= BASE_URL('administrator/ppatk_vaksin_daftar_belum_registrasi') ?>" class="small-box-footer">
          <div class="info-box mb-3">
             <p class="info-box-icon bg-success elevation-1"><?php echo $total_belum_registrasi; ?></p>
            <div class="info-box-content">
              <span class="info-box-text"><b>BELUM REGISTRASI</b></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </a>
      </div>
   </div>
</section>

<!-- Page script -->
<script>
  $(document).ready(function(){

    $('#example').DataTable( {
        "order": [[ 1, "asc" ]],
        "pageLength": 100
    } );

  }); /*end doc ready*/

    $('.remove-data').click(function(){

      var url = $(this).attr('data-href');

      swal({
          title: "Apakah Data Akan Dihapus ??",
          // text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;
          }
        });

      return false;
    });

</script>
