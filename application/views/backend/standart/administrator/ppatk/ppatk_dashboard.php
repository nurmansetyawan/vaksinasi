<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<style>
p {
  color: white !important;
}

h3 {
  color: white !important;
}
span {
  color: white !important;
  text-align: center;
  font-size: 60px;
  margin-top: 0px;
}
</style>
<!-- Main content -->
<section class="content">
  <div class="row">
    <h2 class="text-center">Selamat Datang Di Dashboard Vaksinasi PPATK</h2>
  </div><br>

    <!-- mulai row -->
    <div class="row">
      <div class="col-lg-2 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
          <div class="inner">
            <?php
              $total_sudah_registrasi = $this->db->select('*')->from('v_registrasi')->count_all_results();
              $total_belum_registrasi = $this->db->select('*')->from('v_belum_registrasi')->count_all_results();
            ?>
            <h3><?php echo $total_sudah_registrasi . ' / ' . $total_belum_registrasi ?></h3>

            <p><b>TELAH HADIR / BELUM HADIR</b></p>
          </div>
          <div class="icon">
            <i class="ion ion-ios-people"></i>
          </div>
          <a href="<?= BASE_URL('administrator/ppatk_vaksin_daftar_registrasi') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->

        <div class="col-lg-2 col-6">
          <!-- small box -->
          <div class="small-box bg-secondary">
            <div class="inner">
              <h3><?php echo $total_registrasi = $this->db->select('*')->from('v_lobby')->count_all_results(); ?></h3>

              <p><b>ANTRIAN LOBBY</b></p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-timer"></i>
            </div>
            <a href="<?= BASE_URL('administrator/ppatk_vaksin_daftar_lobby') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->


        <div class="col-lg-2 col-6">
          <!-- small box -->
          <div class="small-box bg-primary">
            <div class="inner">
              <h3><?php echo $total_venue = $this->db->select('*')->from('v_venue')->count_all_results(); ?></h3>

              <p><b>DI VENUE</b></p>
            </div>
            <div class="icon">
              <i class="ion ion-thermometer"></i>
            </div>
              <a href="<?= BASE_URL('administrator/ppatk_vaksin_daftar_venue') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

        <!-- ./col -->
        <div class="col-lg-2 col-6">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h3><?php echo $total_vaksinasi = $this->db->select('*')->from('v_vaksinasi')->count_all_results(); ?></h3>

              <p><b>TELAH VAKSINASI</b></p>
            </div>
            <div class="icon">
              <i class="ion ion-erlenmeyer-flask"></i>
            </div>
              <a href="<?= BASE_URL('administrator/ppatk_vaksin_daftar_vaksinasi') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-2 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3><?php echo $total_tunda = $this->db->select('*')->from('v_tunda')->count_all_results(); ?></h3>

              <p><b>DITUNDA</b></p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
              <a href="<?= BASE_URL('administrator/ppatk_vaksin_daftar_tunda') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3><?php echo $total_observasi= $this->db->select('*')->from('v_observasi')->count_all_results(); ?></h3>

              <p><b>SELESAI OBSERVASI</b></p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-people"></i>
            </div>
              <a href="<?= BASE_URL('administrator/ppatk_vaksin_daftar_observasi') ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
      <center>
        <img src="<?= BASE_URL('uploads/depan.png') ?>" style="width: 20%; height: 20%">
      </center>
    </div>
    <br>
    <div class="row">
      <center>
        <div class="card text-white bg-dark mb-3" style="max-width: 90rem;">
          <div class="card-header">Waktu Vaksin</div>
          <div class="card-body">
            <span id="demo"></span>
          </div>
        </div>
      </center>
    </div>
</section>
<script>
// Set the date we're counting down to
var countDownDate = new Date("Jun 16, 2021 16:00:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
 var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Output the result in an element with id="demo"
  document.getElementById("demo").innerHTML = days + " hari : " + hours + " jam : "
  + minutes + " menit : " + seconds + " detik ";

  // If the count down is over, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
</script>
