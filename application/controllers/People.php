<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/Format.php';

class People extends REST_Controller {

    function __construct(){
         parent:: __construct();
     }
     function index_get(){
         $people = $this->db->get('people')->result();
         $this->response($people, 200);
     }

}
?>
