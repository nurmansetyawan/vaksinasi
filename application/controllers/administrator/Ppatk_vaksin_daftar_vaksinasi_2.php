<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Ppatk_vaksin_daftar_vaksinasi_2 extends Admin
{
	public function __construct()
	{
		parent::__construct();
	}
	// Akhir Fungsi Cek Hak Akses

	// Fungsi Halaman Dashboard
	public function index()
	{
		$this->template->title('Daftar Vaksinasi');
		$this->layout('backend/standart/administrator/ppatk/ppatk_vaksin_daftar_vaksinasi_2');
	}
	// Akhir Fungsi Halaman Dashboard
}
