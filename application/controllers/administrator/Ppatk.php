<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*| --------------------------------------------------------------------------
*| Ppatk Controller
*| --------------------------------------------------------------------------
*| For authentication
*|
*/
class Ppatk extends Admin
{

	public function __construct()
	{
		parent::__construct();
	}

	/**
	* Login user
	*
	*/
	public function index()
	{
		redirect(base_url());
	}

	public function ppatk_check()
	{
		redirect(base_url('administrator/ppatk_dashboard'));
	}
}
