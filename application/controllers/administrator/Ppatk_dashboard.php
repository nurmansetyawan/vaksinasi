<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Ppatk_dashboard extends Admin
{
	public function __construct()
	{
		parent::__construct();
	}
	// Akhir Fungsi Cek Hak Akses

	// Fungsi Halaman Dashboard
	public function index()
	{
		$this->template->title('Dashboard');
		$this->layout('backend/standart/administrator/ppatk/ppatk_dashboard');
	}
	// Akhir Fungsi Halaman Dashboard
}
