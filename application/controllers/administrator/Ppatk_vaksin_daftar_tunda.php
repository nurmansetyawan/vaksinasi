<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Ppatk_vaksin_daftar_tunda extends Admin
{
	public function __construct()
	{
		parent::__construct();
	}
	// Akhir Fungsi Cek Hak Akses

	// Fungsi Halaman Dashboard
	public function index()
	{
		$this->template->title('Daftar Tunda');
		$this->layout('backend/standart/administrator/ppatk/ppatk_vaksin_daftar_tunda');
	}
	// Akhir Fungsi Halaman Dashboard
}
