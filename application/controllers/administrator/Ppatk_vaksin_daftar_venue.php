<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Ppatk_vaksin_daftar_venue extends Admin
{
	public function __construct()
	{
		parent::__construct();
	}
	// Akhir Fungsi Cek Hak Akses

	// Fungsi Halaman Dashboard
	public function index()
	{
		$this->template->title('Daftar Venue');
		$this->layout('backend/standart/administrator/ppatk/ppatk_vaksin_daftar_venue');
	}
	// Akhir Fungsi Halaman Dashboard
}
