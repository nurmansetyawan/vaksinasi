<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route_path = APPPATH . 'routes/';
require_once $route_path . 'routes_landing.php';

$route['404_override'] = 'not_found';
$route['translate_uri_dashes'] = FALSE;

$route['administrator/adminback'] = 'administrator/auth/login';
$route['administrator/register'] = 'administrator/auth/register';
$route['administrator/forgot-password'] = 'administrator/auth/forgot_password';

$route['page/(:any)'] = 'page/detail/$1';
$route['blog/index'] = 'blog/index';
$route['blog/(:any)'] = 'blog/detail/$1';
$route['administrator/web-page'] = 'administrator/page/admin';
